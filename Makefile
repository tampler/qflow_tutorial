#!/bin/bash

# This contains a full tutorial implemenation of QFlow for OSU 035 process

TOP=map9v3

syn:
	@qflow synthesize $(TOP)

lec:
	@yosys source/lec0.ys

place:
	@qflow place $(TOP)

sta:
	@qflow sta $(TOP)

route:
	@qflow route $(TOP)

back:
	@qflow backanno $(TOP)

bld:
	@qflow build $(TOP)

# Fixme: Run in a tcl shell
magic:
	@#magic -tcl layout/magic.tcl 
	@magic -d OGL $(TOP)

# Fixme: Run in a tcl shell
lvs: layout/$(TOP).spice synthesis/$(TOP).spc
	@#netgen -tcl lvs.tcl
	@netgen -batch lvs "layout/$(TOP).spice $(TOP)" "synthesis/$(TOP).spc $(TOP)"

gatesim:
	@iverilog verisim/osu035_stdcells.v synthesis/$(TOP)_synth.rtlnopwr.v verisim/tb.v -o verisim/simv
	@vvp verisim/simv 
	@gtkwave verisim/gate.vcd &

irsim:
	@irsim lvssim/$(TOP).tcl

spicesim:
	@ngspice spice/$(TOP)_cosim_tb.spi

rep:
	@gvim -p log/*

clean:
	@find layout/ -type f ! -name "*.tcl" -delete
	@rm -rf synthesis/* log
	@rm -f *.blif
	@rm -f source/*.blif layout/.magicrc *.out verisim/*.vcd verisim/simv
