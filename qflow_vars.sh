#!/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/bku/work/ndyne/qflow_tutorial
#-------------------------------------------

set projectpath=/home/bku/work/ndyne/qflow_tutorial
set techdir=/usr/local/share/qflow/tech/osu035
set sourcedir=/home/bku/work/ndyne/qflow_tutorial/source
set synthdir=/home/bku/work/ndyne/qflow_tutorial/synthesis
set layoutdir=/home/bku/work/ndyne/qflow_tutorial/layout
set techname=osu035
set scriptdir=/usr/local/share/qflow/scripts
set bindir=/usr/local/share/qflow/bin
set logdir=/home/bku/work/ndyne/qflow_tutorial/log
#-------------------------------------------

