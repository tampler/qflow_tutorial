
# Common setup
set TOP map9v3
set QFLOW /usr/local/share/qflow
set LEF $QFLOW/tech/osu035/osu035_stdcells.lef

# Add cells path
set LIBS ~/work/libs/osu/cadence/lib/source/magic
addpath $LIBS

# Magic setup
box 0 0 0 0
lef read $LEF
def read $TOP.def
grid 1.6um 2.0um
snap grid 

# Generate lvs and spice models
label vdd n m1 
label gnd n m1 
writeall forc $TOP

# Extract parasitics
extract all

# Export to Spice
ext2spice hierarchy on
ext2spice scale off
ext2spice renumber off
ext2spice cthresh infinite
ext2spice rthresh infinite
ext2spice 

# Export to IRSim
ext 2 sim

quit
